# technical-training

## Pattern

### Singleton

![](https://sourcemaking.com/files/v2/content/patterns/singleton1.png)

[Référence](https://sourcemaking.com/design_patterns/singleton)

Problème à résoudre: Application needs one, and only one, instance of an object. Additionally, lazy initialization and global access are necessary.

Objectifs:

    * Ensure a class has only one instance, and provide a global point of access to it.
    * Encapsulated "just-in-time initialization" or "initialization on first use".


On a une classe qui contient un membre privé statique pointant vers l'unique instance de cette classe.

L'accesseur est une méthode publique, qui peut éventuellement créer l'instance lors de la première utilisation (lazyloading)

Check list:

    * Define a private static attribute in the "single instance" class.
    * Define a public static accessor function in the class.
    * Do "lazy initialization" (creation on first use) in the accessor function.
    * Define all constructors to be protected or private.
    * Clients may only use the accessor function to manipulate the Singleton.


### Proxy

![](https://sourcemaking.com/files/v2/content/patterns/Proxy1.png)

[Référence](https://sourcemaking.com/design_patterns/proxy)

Problème à résoudre: You need to support resource-hungry objects, and you do not want to instantiate such objects unless and until they are actually requested by the client.

Objectifs:


    * Provide a surrogate or placeholder for another object to control access to it.
    * Use an extra level of indirection to support distributed, controlled, or intelligent access.
    * Add a wrapper and delegation to protect the real component from undue complexity.

Check list:

    * Identify the leverage or "aspect" that is best implemented as a wrapper or surrogate.
    * Define an interface that will make the proxy and the original component interchangeable.
    * Consider defining a Factory that can encapsulate the decision of whether a proxy or original object is desirable.
    * The wrapper class holds a pointer to the real class and implements the interface.
    * The pointer may be initialized at construction, or on first use.
    * Each wrapper method contributes its leverage, and delegates to the wrappee object.

### Factory method

![](https://sourcemaking.com/files/v2/content/patterns/Factory_Method.png)

[Référence](https://sourcemaking.com/design_patterns/factory_method)

Problème à résoudre: A framework needs to standardize the architectural model for a range of applications, but allow for individual applications to define their own domain objects and provide for their instantiation.

Objectifs:

    * Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.
    * Defining a "virtual" constructor.
    * The new operator considered harmful.

Une implémentation alternative est d'utiliser une méthode statique dans la classe de base.

![](https://sourcemaking.com/files/v2/content/patterns/Factory_Method_1.png)

Check list:

    * If you have an inheritance hierarchy that exercises polymorphism, consider adding a polymorphic creation capability by defining a static factory method in the base class.
    * Design the arguments to the factory method. What qualities or characteristics are necessary and sufficient to identify the correct derived class to instantiate?
    * Consider designing an internal "object pool" that will allow objects to be reused instead of created from scratch.
    * Consider making all constructors private or protected.


### Abstract Factory

![](https://sourcemaking.com/files/v2/content/patterns/Abstract_Factory.png)

[Référence](https://sourcemaking.com/design_patterns/abstract_factory)

Problème à résoudre: If an application is to be portable, it needs to encapsulate platform dependencies. These "platforms" might include: windowing system, operating system, database, etc. Too often, this encapsulation is not engineered in advance, and lots of #ifdef case statements with options for all currently supported platforms begin to procreate like rabbits throughout the code.

Objectifs:


    * Provide an interface for creating families of related or dependent objects without specifying their concrete classes.
    * A hierarchy that encapsulates: many possible "platforms", and the construction of a suite of "products".
    * The new operator considered harmful.

Check list:


    * Decide if "platform independence" and creation services are the current source of pain.
    * Map out a matrix of "platforms" versus "products".
    * Define a factory interface that consists of a factory method per product.
    * Define a factory derived class for each platform that encapsulates all references to the new operator.
    * The client should retire all references to new, and use the factory methods to create the product objects.



## Git

### Merge vs Rebase

![](https://jeffkreeftmeijer.com/git-rebase/git-rebase.png)

Un merge va créer un commit de merge qui va avoir pour parent les 2 commits des deux branches qui ont étés fusionnés

Rebase va **réecrire** les commits de la branche en partant de la branche cible. Ces commits contiennent les même modifications mais ce sont des **nouveaux commits**. Cela veut dire qu'il faudra push force si la branche avait déjà été push par le passé. C'est donc à utiliser sur des branches personelles ou avec une communication sans faille.

L'avantage du rebase est de pouvoir **fast-forward** la branche vers laquelle on a rebase pour la mettre à jour sans faire de commit de merge. Cela garde un historique de commit linéaire.

## Java

### Orienté Objet

Orienté objet veut dire que l'on a des notions comme:

* l'héritage
* l'encapsulation: exposer uniquement des méthodes spécifiques et privatiser les attribut
* le polymorphisme: Un objet peut jouer le rôle d'un autre dans sa hiérarchie d'héritage.

![](https://cdn2.howtodoinjava.com/wp-content/uploads/solid_class_design_principles.png)

### Nouveautés majeures

#### Java 5

* **Programmation générique**
* **Autoboxing/unboxing**
* Enumérations
* Import statiques
* varargs

#### Java 6

Rien de marquant

#### Java 7

* multicatch
* java.nio
* switch avec String
* inférence type : `List<String> = new ArrayList<>();`

#### Java 8

* Programmation fonctionnelle: lambdas, stream
* Optional
* default dans les interfaces

#### Java 9

* Modules (projet Jigsaw)

#### Java 10

* Inférence type variable locale

#### Java 11

#### Java 12

* Expressions switch

### Collections:

![](https://static.javatpoint.com/images/java-collection-hierarchy.png)

* List: Collection ordonnée
	* **ArayList, LinkedList** etc...: implémentation qui font varier la complexité selon les opérations
* Set: collection non ordonnée sans doublons
	* **Hashset**
	* **TreeSet** : ordonné
* Queue:
	* Deque: Subinterface de Queue (Double Ended Queue) à utiliser pour remplacer la classe legacy `Stack` (qui extends Vector qui synchronize toutes les opérations)
		* **ArrayDeque**
		* **LinkedList**
	* **ArrayDeque**
* Map: pas officiellement une "collection", système clé valeurs
	* **HashMap**
	* **TreeMap** : ordonnée

### Mémoire

Java divise sa mémoire entre le **heap** et le **stack**.

Le stack est utilisé pour l'allocation statique de mémmoire et l'exécution d'un thread. Il contient les valeurs primitives locale à une méthode et des références vers des objets du heap. C'est une pile LIFO (last in first out). Quand une méthode est appelée on ajoute un morceau sur le stack, quand elle est terminée, on le retire. Si la mémoire du stack est pleins on obtient une *StackOverFlowError*. Cette mémmoire est rapide par rapport au heap et est threadsafe car chaque thread opere dans son propre stack.

Le heap est utilisé pour de l'allocation dynamique de mémoire pour les objets java au runtime. Les nouveaux objets sont créés dans le heap. Il est divisé en 3 parties

* Young generation : Tous les nouveaux objets. Ceux qui viennent d'être crée sont dans un "eden space" et ceux qui ont survécu au "minor garbage collector" passent dans les "survivor spaces" S0 et S1.
* Old generation: A partir d'un certain age, les objets se déplace de la young gen vers la old gen.
* Permanent generation: Des metadata de la JVM -> retiré dans le JDK 8 et remplacé par meta espace

Si le heap est plein on obtient un *OutOfMemoryError*. Contrairement au stack, la mémoire n'est pas automatique désallouée, il faut le passe de Garbage Collector. Il n'est pas threadsafe.

![](https://www.baeldung.com/wp-content/uploads/2018/07/Stack-Memory-vs-Heap-Space-in-Java.jpg)
![](https://cdn.journaldev.com/wp-content/uploads/2014/05/Java-Memory-Model.png)

### Modificateurs

* volatile: force la JVM à utiliser la valeur de la mémoire principale au lieu d'un cache. Ainsi plusieurs threads auront forcément la même valeur de la variable
* synchronized: indique que la méthode ne peut être exécutée que par un thread à la fois. S'applique à l'objet et non à la classe.
* static: Les attributs et méthodes statiques appartiennent à la classe plutôt qu'à l'objet. Ne peut pas être appelé en dehors d'un contexte static.
* transient: ignoré lors de la sérialisation
* final: ne peut pas être modifié/redefini ou empêche l'héritage d'une classe.
* abstract: ne peut pas être instancié si classe, sinon indique qu'une méthode n'a pas de corps.

### Comparaison d'objet (hashcode/equals)

Pour comparer des objets en java on peut utiliser l'opérateur binaire `==` our la méthode `equals(Object)`.

`==` compare les valeurs pour les types primitifs (int, long...) et ils comparent les adresses dans le cas des objets.

`equals(Object)` est une méthode de Object et peut-être redefinie par n'importe quelle classe.

L'implémentation de `equals(Object)` doit être:

* reflexive: un objet equals lui même -> `o.equals(o) == true`
* symétrique: o1.equals(o2) == o2.equals(o1)
* transitive: o1.equals(o2) et o2.equals(o3) => o1.equals(o3)
* cohérente: pas d'aléatoire, le résultat peut changer si un propriété testée change.

Quand on redefini `equals` il faut aussi redéfinir `hashCode`.

Pourquoi ? Parce que les collections comme HashMap et HashSet classent les objets en les groupant par résultat de hashCode. Si on essaye d'ajouter 2 instances qui sont seulement equals, le hashcode sera différent et on stockera donc le doublon. De plus, on ne pourra pas non plus faire de la recherche dans la collection a partir de cet objet car la recherche est basée sur le `hashCode`.

```java
public int hashCode()
```

`hashCode` doit respecter un contrat avec `equals`. Deux objets `equals` ont le même `hashCode`. Cependant deux objet qui ont le même `hashCode` ne sont pas forcément `equals`.

![](https://www.thejavaprogrammer.com/wp-content/uploads/2017/12/What-is-HashCode-in-Java.jpg)

### Exceptions

![](https://www.javamex.com/tutorials/exceptions/ExceptionHierarchy.png)

Il existe deux types d'exceptions en Java:

* Checked exception: si elle est throw, la méthode doit la `catch` (puis la traiter) ou la `throws` via sa signature. Cette contrainte est vérifiée à la compilation.

* Unchecked exception: il n'y a pas besoin de les traiter ou des les renvoyer explicitement.

"If a client can reasonably be expected to recover from an exception, make it a checked exception. If a client cannot do anything to recover from the exception, make it an unchecked exception"

### Divers

* Les anciennes collections Java étaient threadsafe. Cela peut-être couteux en performance à cause du lock en accès concurent. Les collections récentes (la plupart) ne sont plus threadsafe et délèguent donc la responsabilité au programmeur de gérer l'accès concurent si nécessaire.

## Base de données / Transactions

### ACID

* Atomicity
* Consistency
* Isolation
* Durability

### Jointures

![](https://i.pinimg.com/originals/e3/b8/53/e3b8534ff0b34df8d55f2b81da9e1448.jpg)

### Optimisation requête SQL

Indexation, comment choisir ? Problèmes de l'indexation (taille BD, lenteur d'insertion)

Une base de données peut stocker des **index** sur une colonne pour rechercher rapidement à l'intérieur de celle-ci. Il peut avoir la forme d'une dictionnaire clé-valeur. L'index va permettre de renvoyer toutes les lignes correspondant à la valeur d'une colonne.

Cela permet de ne pas avoir à parcourir l'ensemble d'une table pour retourner le résultat d'une requête.

Ils permettent surtout de gagner du temps quand il y a un nombre considérable de données dans la table (des milliers voir millions).

Généralement, on va choisir d'indexer une colonne qui est utilisé par des clauses WHERE, GROUP BY ou ORDER BY.

Le défaut, c'est que les index prennent de la place dans la base de données. De plus quand on insère une nouvelle valeur il faut mettre à jour les index. 
### Stockage image

Pourquoi utiliser blob ?
[Référence](https://www.quora.com/Is-it-a-bad-design-to-store-images-as-blobs-in-a-database)

* Avantages
	* Fait partie du backup de la db
	* Bonnes performances pour petits fichiers
	* intégrité: on ne risque pas qu'un pointeur vers une image soit faux ou corrompu
	* Si on delete un ligne qui reference une image, l'image peut être delete en cascade. Sinon il faut gerer la suppression sur le système à la main.
* Désavantages
	* Mauvais si on doit souvent accèder aux images et qu'elles sont larges.
	* Ne profite pas des optimisation du système de fichiers
	* Peut rendre la db très lourde

Ce n'est en général pas recommandé d'utiliser des blobs. Il vaut mieux garder les fichiers sur le système et stocker des pointeurs (chemins par exemple) en base de données.

## Hibernate

ORM: Object relational Mapping

| Object                    | Relational        |
|---------------------------|-------------------|
| Classe                    | Table             |
| Objet                     | Ligne             |
| Champs                    | Colonne           |
| @ID                      | Clé primaire      |
|  Relation (@OneToMany...) | Clé étrangère     |
| @...                      | Index             |
| @...                      | Procédure stockée |
| @Property/@NotNull...     | Contrainte |

### Limites d'Hibernate

Hibernate peut êêtre plus lent que des requêtes écrites à la main car le SQL est généré. Il peut parfois y avoir plusieurs requêtes quand une seule serait possible.

Le N+1 problem

La manipulation en tant qu'objet n'est pas toujours adaptée. 

### Héritage

Quand on fait de l'héritage avec des entités Hibernate il faut séléctionner un mode d'organisation des tables dans la base de données.
On utilise `@Inheritance(strategy = InheritanceType.?)`

* Single Table : Une unique table pour toute la hiérarchie. Il faut donc une colonne discriminante pour connaitre le type de l'objet. On peut personnaliser ce discriminant: `@DiscriminatorColumn(name="product_type", discriminatorType = DiscriminatorType.INTEGER)`
* Joined Table: Une table par classe. Si il y a besoin, on utilise des jointures avec clé étrangères sur l'identifiant de la classe parente.
* Table Per Class: Chaque classe à sa table, mais elles contiennent aussi les colonnes des attributs de la classe parente.

Dans la majorité des cas, on préférera Single Table car les champs vides ne sont pas très grave la plupart du temps.

![](https://www.logicbig.com/tutorials/java-ee-tutorial/jpa/table-per-class-inheritance/images/overview.png)
![](https://www.logicbig.com/tutorials/java-ee-tutorial/jpa/joined-table-inheritance/images/overview.png)
![](https://www.logicbig.com/tutorials/java-ee-tutorial/jpa/single-table-inheritance/images/overview.png)

### Relations

Fetch mode par défaut:

* *toMany : lazy
* *toOne : eager

#### Lazyloading

Le lazy-loading est le fait qu'un où plusieurs éléments ne sont pas chargés tant qu'on en fait pas la requête explicite. Cela peut aussi être utilisé pour le pattern Singleton.

Dans le cadre d'Hibernate si on a par exemple un vendeur, on pourrait vouloir que ses articles soit lazy loadés si on ne veut pas tout récupérer à chaque fois qu'on récupère un vendeur.

Cela amène au **problème du n+1**. Avec le lazy loading, si on itère sur la collection d'articles, Hibernate va faire une requête par article au lieu de tous les charger d'un coup.
On peut résoudre ce problème si on utilise `join fetch` en HQL par exemple pour load ce dont on a besoin.
[Plus de détails](https://stackoverflow.com/questions/32453989/what-is-the-solution-for-the-n1-issue-in-jpa-and-hibernate)

Dans les fait, Hibernate utilise un **proxy** (voir section AOP) pour que le chargement se fasse lorsqu'on essaye d'acceder aux éléments proxyfiés.

#### Eagerloading

A l'inverse du lazyloading, on charge l'ensemble de l'objet voulu d'un coup. On n'a pas besoin de nouvelles requêtes pour accèder aux éléments.

Le défaut est un potentiel problème de performance quand on charge plusieurs collections.

### Entity manager
Fonctionnement de la persistence
![](https://i.stack.imgur.com/vLsc6.png)

EntityManager n'est pas threadsafe. Pour gérer la concurence, on peut implémenter du [pessimistic locking](https://martinfowler.com/eaaCatalog/pessimisticOfflineLock.html) ou de l'[optimistic locking](https://martinfowler.com/eaaCatalog/optimisticOfflineLock.html)

Cycle de vie d'un objet (transient, managed, detached)

![](https://thoughts-on-java.org/wp-content/uploads/2017/08/Entity-LifeCycle-State-tiny.png)

La base de données n'est mise à jour que lorsqu'on `flush` pas quand on `persist` ou que l'on `merge`. Le flush peut cependant être en mode automatique. La transaction n'est cependant vraiment stockée définitivement en base de données qu'après un `commit`. Avant le commit elle peut être `rollback`.

L'entity manager est un cache de niveau 1

"A persistence context is a set of entity instances in which for any persistent entity identity there is a unique entity instance. Within the persistence context, the entity instances and their lifecycle are managed." 
In fact, the first level cache is the same as the persistence context. That means operations such as persist(), merge(), remove() are changing only internal collections in the context and are not synchronized to the underlying database. What is the mosts important here is what happens when you invoke the clear() method. It clears the L1 cache. But we know L1 == persistence context. Does it mean clearing L1 removes all entities? In fact yes - all entities will be dropped and never synchronized to the database. That's not a secret, it states in the documentation - "Unflushed changes made to the entity (...) will not be synchronized to the database." ([Source](http://www.kubrynski.com/2017/04/understanding-jpa-l1-caching.html))

Le cache de niveau 2 est au dessus et est commun à tous les persistence context et donc tous les entity manager partageront le cache. Il est par exemple implémenté par EhCache. ([Source](https://www.baeldung.com/hibernate-second-level-cache))




## Spring

On a utilisé Spring 5 et Spring boot 2

Spring à seulement besoin d'un conteneur de servlet

Spring est une alternative a JEE pour les applications d'entreprise.
Plusieurs modules: Security, MVC, Data JPA, Core (DI, IOC, beans)...

### Inversion de contrôle (IOC)

l'inversion de controle est un principe d'architecture logicielle (pour certain c'est un pattern) qui à pour but de laisser le "contrôle" au framework pour réduire le couplage entre les classes.

Concrètement on peut déléguer l'instanciation des dépendances au framework et ne travailler qu'avec des interfaces. **Les instances n'ont plus à se soucier de la façon d'obtenir leur dépendances** => Découplage

L'injection de dépendance est une "implémentation" de l'IOC. On obtient l'IOC parce qu'on utilise l'injection de dépendances (DI). Ce n'est théoriquement pas la seule façon d'atteindre l'IOC. ([référence](https://en.wikipedia.org/wiki/Inversion_of_control#Implementation_techniques))

Avec Spring, c'est le spring container qui à la responsabilité de créer les objets. On peut injecter les objets via le constructeur ou via les setters.

### Application Context

Spring initialise tous les beans dans l'ApplicationContext et gère leur cycle de vie. Les beans sont des "recettes" permettant à Spring de créer des instances d'une classe.
Les beans peuvent avoir différent scope, qui représentent le nombre d'instance qui vont pouvoir être créé et les endroits où elles sont disponibles.

[Source](https://docs.spring.io/spring/docs/3.0.0.M3/reference/html/ch04s04.html)

* singleton: une seule instance par conteneur Spring
* prototype: autant d'instance que nécessaire
* (only Web) request: une instance pour chaque requête HTTP
* (only Web) session: une instance par session HTTP
* (only Web) global session: une instance par session HTTP globale

### Spring VS Spring Boot

TODO

### Spring MVC

TODO

Dispatcher Servlet

### AOP
[Source](https://docs.spring.io/spring/docs/4.3.x/spring-framework-reference/html/aop.html#aop)

Les aspects permettent la modularisation de "crosscutting concerns" qui ne sont pas spécifiques à une classe en particulier.

Dans Spring l'AOP est utilisée à la fois pour fournir des fonctionalités de façon déclaratives avec des annotations (comme @Transactionnal) mais aussi pour permettre aux programmeurs d'implémenter des aspects custom.

### Terminologie

* **Aspect**: Une modularisation de concern qui recoupe plusieurs classes. L'example de la gestion de transaction en est un exemple. Dans Spring, les aspects sont implémentés par des classes ou des classes annotés avec @Aspect (pour @AspectJ).
* **Join point**: Un point durant l'exécution d'un programme (comme l'exécution d'une méthode ou la gestion d'une exceptions).
* **Advice**: Action réalisée par un aspect sur un join point. Les advices ont des types comme "around", "before", "after". On parle d'"Advised object".
* **Pointcut**: Un prédicat qui match certains join points. Un advice peut être run si un pointcut (expression) match un certain join point (exemple: exécution d'une méthode qui porte un certain nom).
* Introduction: Déclarer des méthodes ou des champs derrière un type. L'AOP permet d'ajouter de nouvelles interfaces et leur implémentation sur n'importe quel objet.
* Target object: Un objet qui est concerné par un pou plusieurs aspects. Ils seront proxifiés car dans Spring on utilise des proxy pour l'AOP.
* AOP proxy: Un objet crée par le framework AOP qui implémente les aspects. Dans Spring on utilise un JDK dynamic proxy ou un CGLIB proxy.
* Weaving: Relier des aspects avec des objets pour créer des "advised object" -> concrètement injecte le comportement dans le code

Types d'advice:

* Before: S'exécute avant un join point
* After returning: Après la complétion normale d'un join point (méthode terminée sans lancer d'exception)
* After throwing: Si une exception est lancée
* After (finally): S'exécute à la fin quelque soit la raison (exécution normale ou exception)
* **Around: Peut effectuer des opération avant et après l'invocation d'une méthode. Peut aussi empêcher la méthode de s'exécuter et retourner une autre valeur ou une exception**

### Spring AOP et AspectJ 

Avec Spring, l'AOP supporte uniquement les exécutions de méthodes en "join point". AspectJ peut aller plus loins et intercepter des mise à jour de propriété par exemple.
@AspectJ représente un style de déclaration d'aspect qui utilise des classes java avec des annotations. On peut activer @EnableAspectJAutoProxy sur la configuration pour avoir le support complet de AspectJ et ne pas se limiter à Spring AOP.

Spring AOP supporte uniquement le designateur `execution` pour les pointcuts. Avec AspectJ on a aussi accès à `call, get, set, preinitialization, staticinitialization, initialization, handler, adviceexecution, withincode, cflow, cflowbelow, if, @this`

Example aspect:
```java
package com.journaldev.spring.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class EmployeeAspectPointcut {

	@Before("getNamePointcut()")
	public void loggingAdvice(){
		System.out.println("Executing loggingAdvice on getName()");
	}
	
	@Before("getNamePointcut()")
	public void secondAdvice(){
		System.out.println("Executing secondAdvice on getName()");
	}
	
	@Pointcut("execution(public String getName())")
	public void getNamePointcut(){}
	
	@Before("allMethodsPointcut()")
	public void allServiceMethodsAdvice(){
		System.out.println("Before executing service method");
	}
	
	//Pointcut to execute on all the methods of classes in a package
	@Pointcut("within(com.journaldev.spring.service.*)")
	public void allMethodsPointcut(){}
	
}
```

[Plus d'example](https://www.mkyong.com/spring3/spring-aop-aspectj-annotation-example/)

### proxy

![](https://docs.spring.io/spring/docs/4.3.x/spring-framework-reference/html/images/aop-proxy-call.png)

Lorsqu'on utilise des aspects, Spring remplace les objets par des proxys. Ainsi lorsqu'une méthode est invoquée, elle est invoquée par le proxy, ce qui lui permet de réailiser les opérations des advices avant/après la réelle exécution. Par contre, une fois que le proxy appelle la méthode de l'objet original il n'y a plus de contrôle. Cela veut dire que si une méthode appelle elle même une autre méthode du même objet, alors cet appel ne passera pas par le proxy et ne profitera pas des aspects. Ce problème de self-invocation ne concerne pas AspectJ car ce framework AOP n'est pas basé sur des proxys.

### Transactions

[Référence](https://docs.spring.io/spring/docs/4.2.x/spring-framework-reference/html/transaction.html)

Spring utilise son propre système de transaction pour remplacer celui de JTA (qui utilise des transactions locales et globales)

La stratégie de transaction est définie par l'interface `PlatformTransactionManager` (qui peut avoir différente impl)

Avec l'AOP, Spring permet de gérer les transactions de façon déclarative. `@EnableTransactionManagement` dans la configuration `@Transactional` sur une méthode va faire créer un proxy sur l'objet qui s'occupera de créer la transaction à l'entrée de la méthode et de la commit ou la rollback à la fin.

![](https://docs.spring.io/spring/docs/4.2.x/spring-framework-reference/html/images/tx.png)

## Rest

REST = REpresentational State Transfer

C'est un style d'architecture pour des services Web.

5 principes:

D'après Internet:

1. Client-serveur : Responsabilités séparé client-serveur. L'interface utilisateur et l'API doivent être découplés.
2. Stateless
3. Avec mise en cache: les réponsens doivent pouvoir se définir comme pouvant être mise en cache ou pas.
4. En couches: Le client ne peut pas savoir si il est connecté à un serveur final ou un intermédiaire
5. Interface uniforme: 
	* Identification ressources dans les requêtes (URI)
	* Manipulation ressources via représentation (JSON, XML...)
	* Messages auto-descriptifs: Assez d'information pour savoir comment interpréter.
	* HATEOAS : Hypermedia As The Engine of Application State : Possibilité d'utiliser des hyperliens pour découvrir des actions ou des ressources à partir d'une certaine ressources

Vus avec Olivier:

1. Tout est ressource: URI unique comme identifiant d'une ressource
2. Interface normalisée: utilisation des verbes HTTP pour identifier les opérations
3. Stateless: Le serveur ne stocke pas d'information sur les dernières requêêtes. Chaque requête est indépendante. (y compris pour l'authentification)
4. Négociation de contenus: La réponse n'est pas une ressource mais la représentation d'une ressource (HTML, XML, JSON). Négociation via l'entête Accept.
5. HATEOAS: les ressources contiennent des liens permettant d'accèder à des ressources liées.

Avec REST, certaines opérations respecte l'idempotence, c'est à dire qu'elle renverront toujours le mêême résultat qq soit le nombre de requêêtes (GET, PUT, PATCH)

### Cache

Pour les données à garder longtemps en cache on peut utiliser les ETags (Entity Tags). Pour une même version de la donnée, on aura le même ETag dans le header. Pour les prochaines requêêtes on ajoute le header "If-none-Match" avec l'ancien ETag et le serveur répondra 304 avec un body vide si la data n'a pas changée.

## Application haute disponibilité (high availability - HA)

Le principe de la haute disponibilité est de maintenir les systèmes en état de marche au maximum même dans les cas les plus désastreux.

Cela revient à réduire au maximum le downtime.

En cas d'échec, il faut aussi être capable de remettre le système en marche le plus rapidement possible. On en déduit que l'automatisation est importante dans ce genre d'architecture.

Voici des pistes:

* Redondance: déployer les applications sur plusieurs serveurs. Si un serveur tombe, un autre est disponible. Cela permet aussi de réduire la charge par serveur individuel, ce qui réduit les chance de ralentissement ou de crash.
* Auto-scaling: aujourd'hui le cloud permet d'augmenter ou diminuer les nombre de serveurs selon la charge.
* Slaves: Si on ne peut pas avoir de redondance, on peut envisager un cluster avec des esclaves prêt à prendre la place du maître. (Pour une base de données par exemple)
* Ne pas mettre tous les serveurs au même emplacement physique
* Possèder des backups automatiques: pour pouvoir restaurer des serveurs ou des bases de données.
* Kubernetes

Comment atteindre une application si elle a plusieurs serveurs ? **Utiliser un ou des load balancers** avec le DNS configuré pour répartir la charge sur les différents serveurs.